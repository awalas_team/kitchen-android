package pl.awalas.kitchen.app.utils

import android.content.Context
import android.util.Log
import android.widget.Toast
import pl.awalas.kitchen.app.R
import java.io.IOException

fun downloadError(exception: Throwable, tag: String?, context: Context) {
    Log.d(tag, exception.message.toString())

    if (exception is IOException)
        Toast.makeText(
            context,
            context.getString(R.string.error_check_internet_connection),
            Toast.LENGTH_LONG
        ).show()
    else
        Toast.makeText(
            context,
            context.getString(R.string.error_try_again),
            Toast.LENGTH_LONG
        ).show()
}
