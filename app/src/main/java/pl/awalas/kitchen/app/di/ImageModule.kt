/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.app.di

import android.app.Application
import com.squareup.picasso.OkHttp3Downloader
import com.squareup.picasso.Picasso
import okhttp3.OkHttpClient
import org.koin.dsl.module.module

val imageModule = module {
	single { createOkhttp3Downloader(get()) }
	factory { createPicasso(get(), get()) }
}

fun createOkhttp3Downloader(okHttpClient: OkHttpClient) = OkHttp3Downloader(okHttpClient)

fun createPicasso(application: Application, okHttp3Downloader: OkHttp3Downloader) =
		Picasso.Builder(application.applicationContext).downloader(okHttp3Downloader).build()
