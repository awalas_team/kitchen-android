/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.app.di

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.dsl.module.module
import pl.awalas.kitchen.app.BuildConfig
import pl.awalas.kitchen.app.di.Properties.CACHE_SIZE
import pl.awalas.kitchen.app.di.Properties.TIMEOUT
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val networkModule = module {
	single { createHttpLoggingInterceptor() }
	single { createGsonConverterFactory(get()) }
	single { createGson() }
	single { createRxJava2CallAdapterFactory() }
	single { createOkhttpCache(get()) }
	single { createOkHttpClient(get(), get()) }
	single { createRetrofit(get(), get(), get()) }
}

fun createRetrofit(
	gsonConverterFactory: GsonConverterFactory,
	rxJava2CallAdapterFactory: RxJava2CallAdapterFactory,
	okHttpClient: OkHttpClient
) = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
		.addConverterFactory(gsonConverterFactory)
		.addCallAdapterFactory(rxJava2CallAdapterFactory)
		.client(okHttpClient)
		.build()

fun createRxJava2CallAdapterFactory() = RxJava2CallAdapterFactory.create()

fun createOkHttpClient(cache: Cache, httpLoggingInterceptor: HttpLoggingInterceptor) =
		OkHttpClient.Builder()
				.cache(cache)
				.addInterceptor(httpLoggingInterceptor)
				.connectTimeout(TIMEOUT, TimeUnit.SECONDS)
				.writeTimeout(TIMEOUT, TimeUnit.SECONDS)
				.readTimeout(TIMEOUT, TimeUnit.SECONDS).build()

fun createOkhttpCache(application: Application): Cache {
	val cacheSize = CACHE_SIZE
	return Cache(application.applicationContext.cacheDir, cacheSize.toLong())
}

fun createGson() = GsonBuilder().setDateFormat("yyyy-MM-dd' 'HH:mm:ss").create()

fun createGsonConverterFactory(gson: Gson) = GsonConverterFactory.create(gson)

fun createHttpLoggingInterceptor(): HttpLoggingInterceptor {
	val logging = HttpLoggingInterceptor()
	logging.level = HttpLoggingInterceptor.Level.BODY
	return logging
}

object Properties {
	const val TIMEOUT = 30L
	const val CACHE_SIZE = 10 * 1024 * 1024 // 10 MB
}
