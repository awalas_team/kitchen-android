/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.app.di

import org.koin.dsl.module.module
import pl.awalas.kitchen.data.web.api.dishandproduct.product.KitchenUnitApi
import pl.awalas.kitchen.data.web.api.dishandproduct.product.ProductApi
import pl.awalas.kitchen.data.web.service.dishandproduct.product.KitchenUnitApiService
import pl.awalas.kitchen.data.web.service.dishandproduct.product.ProductApiService
import retrofit2.Retrofit

val apiModule = module {
    single { createProductWebApiService(get()) }
    single { createUnitWebApiService(get()) }
}

fun createProductWebApiService(restAdapter: Retrofit) =
    ProductApiService(restAdapter.create<ProductApi>(ProductApi::class.java))

fun createUnitWebApiService(restAdapter: Retrofit) =
    KitchenUnitApiService(restAdapter.create<KitchenUnitApi>(KitchenUnitApi::class.java))
