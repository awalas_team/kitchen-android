/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.app

import android.app.Application
import org.koin.android.ext.android.startKoin
import pl.awalas.kitchen.app.di.apiModule
import pl.awalas.kitchen.app.di.daoModule
import pl.awalas.kitchen.app.di.fragmentModule
import pl.awalas.kitchen.app.di.imageModule
import pl.awalas.kitchen.app.di.networkModule
import pl.awalas.kitchen.app.di.repositoryModule
import pl.awalas.kitchen.app.di.storageModule
import pl.awalas.kitchen.app.di.useCaseModule
import pl.awalas.kitchen.app.di.viewModelModule

open class App: Application() {
	
	override fun onCreate() {
		super.onCreate()
		
		startKoin(this, listOf(networkModule, storageModule, daoModule, imageModule, apiModule,
		                       repositoryModule, useCaseModule, viewModelModule, fragmentModule))
	}
}
