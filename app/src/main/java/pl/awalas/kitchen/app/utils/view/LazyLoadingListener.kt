package pl.awalas.kitchen.app.utils.view

import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

abstract class LazyLoadingListener : RecyclerView.OnScrollListener {

    private var visibleThreshold = START_VISIBLE_THRESHOLD
    private var currentPage = 0
    var loading = true

    private var mLayoutManager: RecyclerView.LayoutManager

    constructor(layoutManager: LinearLayoutManager) {
        this.mLayoutManager = layoutManager
    }

    constructor(layoutManager: GridLayoutManager) {
        this.mLayoutManager = layoutManager
        visibleThreshold *= layoutManager.spanCount
    }

    override fun onScrolled(view: RecyclerView, dx: Int, dy: Int) {
        if (dy > 0) {
            val totalItemCount = mLayoutManager.itemCount

            val lastVisibleItemPosition = when (mLayoutManager) {
                is GridLayoutManager ->
                    (mLayoutManager as GridLayoutManager).findLastVisibleItemPosition()
                is LinearLayoutManager ->
                    (mLayoutManager as LinearLayoutManager).findLastVisibleItemPosition()
                else -> 0
            }

            if (!loading && lastVisibleItemPosition + visibleThreshold > totalItemCount) {
                currentPage++
                onLoadMore(currentPage)
                loading = true
            }
        }
    }

    fun resetState() {
        currentPage = 0
        loading = true
    }

    abstract fun onLoadMore(page: Int)

    companion object {
        private const val START_VISIBLE_THRESHOLD = 5
    }
}
