package pl.awalas.kitchen.app.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_products_list.product_list
import kotlinx.android.synthetic.main.fragment_products_list.swipe_layout
import org.koin.android.viewmodel.ext.android.viewModel
import pl.awalas.kitchen.app.R
import pl.awalas.kitchen.app.utils.view.LazyLoadingListener
import pl.awalas.kitchen.app.utils.view.inflate
import pl.awalas.kitchen.app.view.adapter.ProductsListRecyclerViewAdapter
import pl.awalas.kitchen.app.viewmodel.ProductsListViewModel

class ProductsListFragment : Fragment() {

    val productsListViewModel: ProductsListViewModel by viewModel()
    private lateinit var adapter: ProductsListRecyclerViewAdapter

    private lateinit var scrollListener: LazyLoadingListener

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return container?.inflate(R.layout.fragment_products_list)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        configureViewModel()

        adapter = ProductsListRecyclerViewAdapter(picasso = Picasso.get())
        product_list.adapter = adapter

        val linearLayoutManager = LinearLayoutManager(context)
        product_list.layoutManager = linearLayoutManager

        scrollListener = object : LazyLoadingListener(linearLayoutManager) {
            override fun onLoadMore(page: Int) {
                productsListViewModel.loadProducts(page * ITEM_ON_SITE)
            }
        }

        product_list.addOnScrollListener(scrollListener)
        swipe_layout.setOnRefreshListener {
            scrollListener.resetState()
            adapter.setData(emptyList())
            productsListViewModel.initProducts()
        }
    }

    private fun configureViewModel() {
        productsListViewModel.initProducts()
        /*productsListViewModel.productOutcome.observe(
            this,
            Observer<Outcome<List<Product>>> { outcome ->
                when (outcome) {

                    is Outcome.Progress -> swipe_layout.isRefreshing = outcome.loading

                    is Outcome.Success -> {
                        adapter.setData(outcome.data)
                        scrollListener.loading = false
                    }

                    is Outcome.Failure -> {
                        downloadError(
                            outcome.e,
                            ProductsListFragment::class.qualifiedName,
                            this.context!!
                        )
                        scrollListener.loading = false
                    }
                }
            })*/
    }

    companion object {
        private const val ITEM_ON_SITE = 20
    }
}
