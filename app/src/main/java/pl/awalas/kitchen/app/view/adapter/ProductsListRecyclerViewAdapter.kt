package pl.awalas.kitchen.app.view.adapter

import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_products_list.view.product_desc
import kotlinx.android.synthetic.main.item_products_list.view.product_name
import kotlinx.android.synthetic.main.item_products_list.view.product_photo
import pl.awalas.kitchen.app.R
import pl.awalas.kitchen.app.utils.view.inflate
import pl.awalas.kitchen.domain.model.dishandproduct.product.Product
import java.util.LinkedList

class ProductsListRecyclerViewAdapter(private val picasso: Picasso)
    : RecyclerView.Adapter<ProductsListRecyclerViewAdapter.ViewHolder>() {

    private var products: MutableList<Product> = LinkedList()
    private var onClickListener: View.OnClickListener

    private var adapterCallback: AdapterCallback? = null

    init {
        onClickListener = View.OnClickListener { v ->
            val item = v.tag as Product
            adapterCallback?.onClickListener(item.id)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = parent.inflate(R.layout.item_products_list)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = products[position]
        val stringTitle = "" + item.id + " " + item.name
        holder.productName.text = stringTitle
        holder.productDesc.text = item.notes
        // picasso.load(item.photo?.sorted().get(0).url).into(holder.productPhoto)

        with(holder.mView) {
            tag = item
            setOnClickListener(onClickListener)
        }
    }

    fun setData(data: List<Product>?) {
        this.products = data?.toMutableList() ?: this.products
        notifyDataSetChanged()
    }

    fun addData(data: List<Product>) {
        val oldSize = itemCount
        this.products.addAll(data.toMutableList())
        notifyItemRangeInserted(oldSize, itemCount - 1)
    }

    fun setListener(listener: AdapterCallback) {
        this.adapterCallback = listener
    }

    override fun getItemCount(): Int = products.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val productName: TextView = mView.product_name
        val productDesc: TextView = mView.product_desc
        val productPhoto: ImageView = mView.product_photo
    }

    interface AdapterCallback {
        fun onClickListener(itemID: Long)
    }
}
