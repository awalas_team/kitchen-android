/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.web.model.dishandproduct.common

import pl.awalas.kitchen.data.utils.mapper.MappableFromWebModel
import pl.awalas.kitchen.domain.model.dishandproduct.common.Nutrients

data class NutrientsWebModel(

    val kcal: Double,
    val protein: Double,
    val carbohydrates: Double,
    val roughage: Double,
    val sugars: Double,
    val fat: Double,
    val cholesterol: Double,
    val sodium: Double,
    val potassium: Double,
    val vitaminA: Double,
    val vitaminB: Double,
    val calcium: Double,
    val iron: Double,
    val saturated: Double,
    val polyunsaturated: Double,
    val monounsaturated: Double,
    val trans: Double,
    val id: Long

) : MappableFromWebModel<Nutrients> {

    override fun mapFromWebModel() = Nutrients(
        kcal,
        protein,
        carbohydrates,
        roughage,
        sugars,
        fat,
        cholesterol,
        sodium,
        potassium,
        vitaminA,
        vitaminB,
        calcium,
        iron,
        saturated,
        polyunsaturated,
        monounsaturated,
        trans,
        id
    )
}
