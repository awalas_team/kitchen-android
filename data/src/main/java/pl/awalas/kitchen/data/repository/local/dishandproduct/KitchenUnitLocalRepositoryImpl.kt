package pl.awalas.kitchen.data.repository.local.dishandproduct

import pl.awalas.kitchen.data.database.model.dishandproduct.product.KitchenUnitEntity
import pl.awalas.kitchen.data.database.service.dishandproduct.product.UnitDaoService
import pl.awalas.kitchen.domain.model.dishandproduct.product.KitchenUnit
import pl.awalas.kitchen.domain.repository.local.dishandproduct.KitchenUnitLocalRepository

class KitchenUnitLocalRepositoryImpl(private val unitDaoService: UnitDaoService) :
    KitchenUnitLocalRepository {

    override fun saveKitchenUnitToDB(kitchenUnit: KitchenUnit) =
        unitDaoService.insert(kitchenUnit.run { KitchenUnitEntity(id, name, timestamp) })

    override fun saveKitchenUnitsToDB(kitchenUnits: List<KitchenUnit>) =
        unitDaoService.insert(kitchenUnits.map { KitchenUnitEntity(it.id, it.name, it.timestamp) })

    override fun getKitchenUnitsFromDB() =
        unitDaoService.getAll().map { list -> list.map { it.mapFromEntity() } }

    override fun getKitchenUnitFromDB(kitchenUnitID: Long) =
        unitDaoService.getSingle(kitchenUnitID).map { it.mapFromEntity() }

    override fun getKitchenUnitForProductFromDB(productId: Long) =
        unitDaoService.getUnitForProduct(productId).map { it.mapFromEntity() }
}
