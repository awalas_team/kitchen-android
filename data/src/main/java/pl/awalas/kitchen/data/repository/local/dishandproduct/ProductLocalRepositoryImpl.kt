/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.repository.local.dishandproduct

import pl.awalas.kitchen.data.database.model.dishandproduct.product.ProductEntity
import pl.awalas.kitchen.data.database.service.dishandproduct.product.ProductDaoService
import pl.awalas.kitchen.domain.model.dishandproduct.product.Product
import pl.awalas.kitchen.domain.repository.local.dishandproduct.ProductLocalRepository

class ProductLocalRepositoryImpl(private val productDaoService: ProductDaoService) :
    ProductLocalRepository {

    override fun saveProductsToDB(products: List<Product>) =
        productDaoService.insert(products.map {
            it.run {
                ProductEntity(
                    id,
                    name,
                    validity,
                    storageDesc,
                    notes,
                    counter,
                    timestamp,
                    ownerID,
                    nutrients?.id,
                    kitchenUnit.id
                )
            }
        })

    override fun saveProductToDB(product: Product) =
        productDaoService.insert(product.run {
            ProductEntity(
                id,
                name,
                validity,
                storageDesc,
                notes,
                counter,
                timestamp,
                ownerID,
                nutrients?.id,
                kitchenUnit.id
            )
        })

    override fun getProductsFromDB(offset: Int, limit: Int) =
        productDaoService.getTop(offset, limit).map { list -> list.map { it.mapFromEntity() } }

    override fun getProductFromDB(productID: Long) =
        productDaoService.getSingle(productID).map { it.mapFromEntity() }

    override fun removeProductsFromDB(products: List<Product>) =
        productDaoService.delete(products.map {
            it.run {
                ProductEntity(
                    id,
                    name,
                    validity,
                    storageDesc,
                    notes,
                    counter,
                    timestamp,
                    ownerID,
                    nutrients?.id,
                    kitchenUnit.id
                )
            }
        })
}
