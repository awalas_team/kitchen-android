/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.database.model.dishandproduct.product

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Ignore
import androidx.room.Index
import androidx.room.PrimaryKey
import pl.awalas.kitchen.data.utils.mapper.MappableFromEntity
import pl.awalas.kitchen.domain.model.dishandproduct.product.Product
import java.util.Date

@Entity(
    tableName = "products", indices = [Index("unitID")],
    foreignKeys =
    [(ForeignKey(
        entity = KitchenUnitEntity::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("unitID")
    ))]
)
data class ProductEntity @JvmOverloads constructor(

    @PrimaryKey
    var id: Long,
    var name: String,
    var validity: String?,
    var storageDesc: String?,
    var notes: String?,
    var counter: Int,
    var timestamp: Date,
    @Ignore
    var ownerID: Long? = 0,
    @Ignore
    var nutrientsID: Long? = 0,
    var unitID: Long

) : MappableFromEntity<Product> {

    override fun mapFromEntity() = Product(
        id,
        name,
        validity,
        storageDesc,
        notes,
        counter,
        timestamp,
        ownerID,
        null,
        null
    )
}
