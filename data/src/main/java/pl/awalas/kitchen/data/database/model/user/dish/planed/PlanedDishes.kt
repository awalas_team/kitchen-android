/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.database.model.user.dish.planed

import pl.awalas.kitchen.data.database.model.dishandproduct.dish.DishType
import java.util.Date
import java.util.TreeSet

// todo planned
class PlanedDishes {

    private val id: Long = 0
    private val day: Date? = null
    private val position: Int = 0
    private val notes: String? = null
    var plandedPortion = TreeSet<PlanedPortion>()
    var dishType: DishType? = null
}
