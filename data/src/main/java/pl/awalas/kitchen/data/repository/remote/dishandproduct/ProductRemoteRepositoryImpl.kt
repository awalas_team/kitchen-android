/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.repository.remote.dishandproduct

import io.reactivex.Single
import pl.awalas.kitchen.data.utils.PathDate
import pl.awalas.kitchen.data.web.service.dishandproduct.product.ProductApiService
import pl.awalas.kitchen.domain.model.dishandproduct.product.Product
import pl.awalas.kitchen.domain.repository.remote.dishandproduct.ProductRemoteRepository
import pl.awalas.kitchen.domain.utils.WebListResponse
import java.util.Date

class ProductRemoteRepositoryImpl(private val productApi: ProductApiService) :
    ProductRemoteRepository {

    override fun getProduct(productID: Long, timestamp: Date) =
        productApi.getProduct(productID, PathDate(timestamp)).map {
            it.mapFromWebModel()
        }

    override fun getProducts(
        offset: Int,
        limit: Int,
        timestamp: Date
    ): Single<WebListResponse<Product>> =
        productApi.getLimitProducts(PathDate(timestamp), offset, limit).map { response ->
            WebListResponse(
                response.newData.map { it.mapFromWebModel() },
                response.removeData.map { it.mapFromWebModel() })

        }
}
