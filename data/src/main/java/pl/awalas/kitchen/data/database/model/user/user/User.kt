/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.database.model.user.user

import pl.awalas.kitchen.data.database.model.diet.Diet
import pl.awalas.kitchen.data.database.model.dishandproduct.dish.Dish
import pl.awalas.kitchen.data.database.model.user.dish.eaten.DrinkWater
import pl.awalas.kitchen.data.database.model.user.dish.eaten.EatenPortion
import pl.awalas.kitchen.data.database.model.user.dish.planed.PlanedDiet
import pl.awalas.kitchen.data.database.model.user.dish.planed.PlanedDishes
import pl.awalas.kitchen.data.database.model.user.shop.Shopping
import pl.awalas.kitchen.data.database.model.user.storage.Storage
import java.util.Date
import java.util.TreeSet

// todo user
class User {

    private val id: Long = 0
    private val name: String? = null
    private val surname: String? = null
    private val birthDate: Date? = null
    private val dietetican: Boolean = false
    var adviser: User? = null
    var client = TreeSet<User>()
    var createdDiet = TreeSet<Diet>()
    var drinkWater = TreeSet<DrinkWater>()
    var userParam = TreeSet<UserParam>()
    var storage = TreeSet<Storage>()
    var planedDiet = TreeSet<PlanedDiet>()
    var dish = TreeSet<Dish>()
    var eatedPortion = TreeSet<EatenPortion>()
    var shopping = TreeSet<Shopping>()
    var planedDishes = TreeSet<PlanedDishes>()
}
