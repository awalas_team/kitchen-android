package pl.awalas.kitchen.data.utils.mapper

interface MappableFromEntity<T> {

    fun mapFromEntity(): T
}
