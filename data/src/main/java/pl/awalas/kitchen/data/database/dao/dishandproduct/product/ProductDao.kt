/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.database.dao.dishandproduct.product

import androidx.room.Dao
import androidx.room.Query
import io.reactivex.Single
import pl.awalas.kitchen.data.database.dao.BaseDao
import pl.awalas.kitchen.data.database.model.dishandproduct.product.ProductEntity

@Dao
interface ProductDao : BaseDao<ProductEntity> {

    @Query("SELECT * FROM products WHERE id = :id")
    fun getSingle(id: Long): Single<ProductEntity>

    @Query("SELECT * FROM products")
    fun getAll(): Single<List<ProductEntity>>

    @Query("SELECT * FROM products ORDER BY counter DESC LIMIT :limit OFFSET :offset")
    fun getTop(offset: Int, limit: Int): Single<List<ProductEntity>>

    @Query("SELECT * FROM products ORDER BY timestamp DESC LIMIT 1")
    fun lastRefresh(): Single<ProductEntity>
}
