/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import pl.awalas.kitchen.data.database.dao.dishandproduct.product.ProductDao
import pl.awalas.kitchen.data.database.dao.dishandproduct.product.UnitDao
import pl.awalas.kitchen.data.database.model.dishandproduct.product.KitchenUnitEntity
import pl.awalas.kitchen.data.database.model.dishandproduct.product.ProductEntity
import pl.awalas.kitchen.data.utils.Converters

@Database(
    entities = [(ProductEntity::class), (KitchenUnitEntity::class)],
    version = 1,
    exportSchema = false
)
@TypeConverters(Converters::class)
abstract class KitchenDatabase : RoomDatabase() {

    abstract fun productDao(): ProductDao

    abstract fun unitDao(): UnitDao
}
