/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.database.dao.dishandproduct.product

import androidx.room.Dao
import androidx.room.Query
import io.reactivex.Single
import pl.awalas.kitchen.data.database.dao.BaseDao
import pl.awalas.kitchen.data.database.model.dishandproduct.product.KitchenUnitEntity

@Dao
interface UnitDao : BaseDao<KitchenUnitEntity> {

    @Query("SELECT * FROM kitchenUnits WHERE id = :id")
    fun getSingle(id: Long): Single<KitchenUnitEntity>

    @Query("SELECT * FROM kitchenUnits")
    fun getAll(): Single<List<KitchenUnitEntity>>

    @Query("SELECT * FROM kitchenUnits ORDER BY timestamp DESC LIMIT 1")
    fun lastRefresh(): Single<KitchenUnitEntity>

    @Query(
        "SELECT kitchenUnits.* FROM kitchenUnits " +
                "INNER JOIN products ON unitID = kitchenUnits.id " +
                "WHERE products.id = :productID"
    )
    fun getUnitForProduct(productID: Long): Single<KitchenUnitEntity>
}
