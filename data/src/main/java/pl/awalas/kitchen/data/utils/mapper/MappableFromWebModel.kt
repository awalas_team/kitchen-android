package pl.awalas.kitchen.data.utils.mapper

interface MappableFromWebModel<T> {

    fun mapFromWebModel(): T
}
