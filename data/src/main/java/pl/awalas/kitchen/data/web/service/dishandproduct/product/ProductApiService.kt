/*
 * ******************************************************************************
 *  * Copyright 2018 Andrzej Walas
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *    http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  *****************************************************************************
 */

package pl.awalas.kitchen.data.web.service.dishandproduct.product

import pl.awalas.kitchen.data.utils.PathDate
import pl.awalas.kitchen.data.web.api.dishandproduct.product.ProductApi
import pl.awalas.kitchen.data.web.model.dishandproduct.product.ProductWebModel

class ProductApiService(private val productApi: ProductApi) {

    fun getProduct(id: Long, timestamp: PathDate) = productApi.getProduct(id, timestamp)

    fun getProducts(timestamp: PathDate) = productApi.getProducts(timestamp)

    fun getLimitProducts(timestamp: PathDate, offset: Int, limit: Int) =
        productApi.getLimitProducts(timestamp, offset, limit)

    fun addProduct(product: ProductWebModel) = productApi.addProduct(product)

    fun editProduct(product: ProductWebModel) = productApi.editProduct(product)

    fun delProduct(id: Long) = productApi.delProducts(id)
}
