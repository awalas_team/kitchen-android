/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.web.api.dishandproduct.product

import io.reactivex.Completable
import io.reactivex.Single
import pl.awalas.kitchen.data.utils.PathDate
import pl.awalas.kitchen.data.web.model.dishandproduct.product.ProductWebModel
import pl.awalas.kitchen.domain.utils.WebListResponse
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ProductApi {

    @GET("/product/")
    fun getProduct(@Query("ID") id: Long, @Query("timestamp") timestamp: PathDate):
            Single<ProductWebModel>

    @GET("/products/")
    fun getProducts(@Query("timestamp") timestamp: PathDate): Single<WebListResponse<ProductWebModel>>

    @GET("/products/limit/")
    fun getLimitProducts(
        @Query("timestamp") timestamp: PathDate,
        @Query("offset") offset: Int,
        @Query("limit") limit: Int
    ): Single<WebListResponse<ProductWebModel>>

    @POST("/product/add")
    fun addProduct(product: ProductWebModel): Single<ProductWebModel>

    @POST("/product/edit")
    fun editProduct(product: ProductWebModel): Single<ProductWebModel>

    @DELETE("/product/")
    fun delProducts(@Query("ID") id: Long): Completable
}
