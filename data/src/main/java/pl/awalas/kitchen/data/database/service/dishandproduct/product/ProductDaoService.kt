/*
 * ******************************************************************************
 *  * Copyright 2018 Andrzej Walas
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *    http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  *****************************************************************************
 */

package pl.awalas.kitchen.data.database.service.dishandproduct.product

import pl.awalas.kitchen.data.database.dao.dishandproduct.product.ProductDao
import pl.awalas.kitchen.data.database.model.dishandproduct.product.ProductEntity

class ProductDaoService(private val productDao: ProductDao) {

    fun insert(productEntity: ProductEntity) = productDao.insert(productEntity)

    fun insert(productEntities: List<ProductEntity>) = productDao.insert(productEntities)

    fun getSingle(id: Long) = productDao.getSingle(id)

    fun getAll() = productDao.getAll()

    fun getTop(offset: Int, limit: Int) = productDao.getTop(offset, limit)

    fun lastRefresh() = productDao.lastRefresh()

    fun delete(productEntity: ProductEntity) = productDao.delete(productEntity)

    fun delete(productEntities: List<ProductEntity>) = productDao.delete(productEntities)
}
