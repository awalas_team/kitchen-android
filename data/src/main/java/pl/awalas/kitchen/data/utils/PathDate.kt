package pl.awalas.kitchen.data.utils

import java.text.SimpleDateFormat
import java.util.Date
import java.util.GregorianCalendar
import java.util.Locale

class PathDate(private val date: Date) {

    override fun toString(): String {
        val cal = GregorianCalendar()
        val dateFormat = SimpleDateFormat("yyyy-MM-dd' 'HH:mm", Locale.ENGLISH)
        dateFormat.timeZone = cal.timeZone
        return dateFormat.format(date)
    }
}
