/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.data.web.model.dishandproduct.product

import pl.awalas.kitchen.data.utils.mapper.MappableFromWebModel
import pl.awalas.kitchen.data.web.model.dishandproduct.common.NutrientsWebModel
import pl.awalas.kitchen.data.web.model.dishandproduct.common.PhotoWebModel
import pl.awalas.kitchen.domain.model.dishandproduct.product.Product
import java.util.Date

data class ProductWebModel(

    val id: Long,
    val name: String,
    val validity: String?,
    val storageDesc: String?,
    val notes: String?,
    val counter: Int,
    val timestamp: Date,
    val ownerID: Long?,
    val photo: List<PhotoWebModel>?,
    val nutrients: NutrientsWebModel?,
    val kitchenUnit: KitchenUnitWebModel

) : MappableFromWebModel<Product> {
    override fun mapFromWebModel(): Product {
        val product = Product(
            id,
            name,
            validity,
            storageDesc,
            notes,
            counter,
            timestamp,
            ownerID,
            photo?.map { it.mapFromWebModel() },
            nutrients?.mapFromWebModel()
        )
        product.kitchenUnit = kitchenUnit.mapFromWebModel()
        return product
    }
}
