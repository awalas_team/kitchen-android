package pl.awalas.kitchen.domain.scheduler

import io.reactivex.CompletableTransformer
import io.reactivex.Maybe
import io.reactivex.MaybeTransformer
import io.reactivex.Observable
import io.reactivex.ObservableTransformer
import io.reactivex.Single
import io.reactivex.SingleTransformer

interface ComposedScheduler {

    /**
     * Apply a thread scheduling [io.reactivex.SingleTransformer] to a single.
     *
     * @param <T> the [Single] type.
     * @return the newly composed single.
    </T> */
    fun <T> scheduleSingle(): SingleTransformer<T, T>

    /**
     * Apply a thread scheduling [io.reactivex.MaybeTransformer] to a maybe.
     *
     * @param <T> the [Maybe] type.
     * @return the newly composed maybe.
    </T> */
    fun <T> scheduleMaybe(): MaybeTransformer<T, T>

    /**
     * Apply a thread scheduling [io.reactivex.CompletableTransformer] to a completable.
     *
     * @return the newly composed completable.
     */
    fun scheduleCompletable(): CompletableTransformer

    /**
     * Apply a thread scheduling [io.reactivex.CompletableTransformer] to an observable.
     *
     * @param <T> the [Observable] type.
     * @return the newly composed observable.
     */
    fun <T> scheduleObservable(): ObservableTransformer<T, T>
}
