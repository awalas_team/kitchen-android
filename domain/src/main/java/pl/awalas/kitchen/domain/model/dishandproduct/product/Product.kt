/*******************************************************************************
 * Copyright 2018 Andrzej Walas
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package pl.awalas.kitchen.domain.model.dishandproduct.product

import pl.awalas.kitchen.domain.model.dishandproduct.common.Nutrients
import pl.awalas.kitchen.domain.model.dishandproduct.common.Photo
import java.util.Date

data class Product(

    var id: Long,
    var name: String,
    var validity: String?,
    var storageDesc: String?,
    var notes: String?,
    var counter: Int,
    var timestamp: Date,
    var ownerID: Long?,
    var photo: List<Photo>? = emptyList(),
    var nutrients: Nutrients? = null

) {
    lateinit var kitchenUnit: KitchenUnit
}
