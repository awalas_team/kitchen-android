package pl.awalas.kitchen.domain.interactor.base

import io.reactivex.Single
import pl.awalas.kitchen.domain.scheduler.ComposedScheduler

abstract class SingleEmptyUseCase<R>(scheduler: ComposedScheduler) : SingleUseCase<Unit, R>(scheduler) {

    fun execute() = super.execute(Unit)

    override fun buildUseCaseObservable(model: Unit) = buildUseCaseObservable()

    protected abstract fun buildUseCaseObservable(): Single<R>
}
