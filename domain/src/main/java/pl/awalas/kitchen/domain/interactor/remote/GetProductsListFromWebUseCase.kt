/*
 * ******************************************************************************
 *  * Copyright 2018 Andrzej Walas
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *    http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  *****************************************************************************
 */

package pl.awalas.kitchen.domain.interactor.remote

import pl.awalas.kitchen.domain.interactor.base.SingleUseCase
import pl.awalas.kitchen.domain.model.dishandproduct.product.Product
import pl.awalas.kitchen.domain.repository.remote.dishandproduct.ProductRemoteRepository
import pl.awalas.kitchen.domain.scheduler.ComposedScheduler
import pl.awalas.kitchen.domain.utils.WebListResponse
import pl.awalas.kitchen.domain.utils.getTemplateDate

class GetProductsListFromWebUseCase(
    scheduler: ComposedScheduler,
    private val productRemoteRepository: ProductRemoteRepository
) : SingleUseCase<Int, WebListResponse<Product>>(scheduler) {

    override fun buildUseCaseObservable(offset: Int) =
        productRemoteRepository.getProducts(offset, 20, getTemplateDate())
}
