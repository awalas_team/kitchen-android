/*
 * ******************************************************************************
 *  * Copyright 2018 Andrzej Walas
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *    http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  *****************************************************************************
 */

package pl.awalas.kitchen.domain.repository.local.dishandproduct

import io.reactivex.Completable
import io.reactivex.Single
import pl.awalas.kitchen.domain.model.dishandproduct.product.Product

interface ProductLocalRepository {

    fun saveProductsToDB(products: List<Product>): Completable

    fun saveProductToDB(product: Product): Completable

    fun getProductsFromDB(offset: Int, limit: Int): Single<List<Product>>

    fun getProductFromDB(productID: Long): Single<Product>

    fun removeProductsFromDB(products: List<Product>): Completable
}
