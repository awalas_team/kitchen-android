/*
 * ******************************************************************************
 *  * Copyright 2018 Andrzej Walas
 *  *
 *  * Licensed under the Apache License, Version 2.0 (the "License");
 *  * you may not use this file except in compliance with the License.
 *  * You may obtain a copy of the License at
 *  *
 *  *    http://www.apache.org/licenses/LICENSE-2.0
 *  *
 *  * Unless required by applicable law or agreed to in writing, software
 *  * distributed under the License is distributed on an "AS IS" BASIS,
 *  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  * See the License for the specific language governing permissions and
 *  * limitations under the License.
 *  *****************************************************************************
 */

package pl.awalas.kitchen.domain.interactor.remote

import pl.awalas.kitchen.domain.interactor.base.SingleUseCase
import pl.awalas.kitchen.domain.model.dishandproduct.product.Product
import pl.awalas.kitchen.domain.repository.local.dishandproduct.ProductLocalRepository
import pl.awalas.kitchen.domain.repository.remote.dishandproduct.ProductRemoteRepository
import pl.awalas.kitchen.domain.scheduler.ComposedScheduler
import pl.awalas.kitchen.domain.utils.getTemplateDate
import java.util.Date

class GetProductFromWebUseCase(
    scheduler: ComposedScheduler,
    private val productRemoteRepository: ProductRemoteRepository
) : SingleUseCase<Long, Product>(scheduler) {

    override fun buildUseCaseObservable(productID: Long) = productRemoteRepository.getProduct(productID, getTemplateDate())
}
