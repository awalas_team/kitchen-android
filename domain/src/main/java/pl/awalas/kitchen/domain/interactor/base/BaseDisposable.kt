package pl.awalas.kitchen.domain.interactor.base

import io.reactivex.disposables.Disposable
import java.util.concurrent.atomic.AtomicBoolean

class BaseDisposable : AtomicBoolean(), Disposable {

    override fun dispose() = set(true)

    override fun isDisposed() = get()
}