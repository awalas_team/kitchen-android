package pl.awalas.kitchen.domain.interactor.base

import io.reactivex.Single
import pl.awalas.kitchen.domain.scheduler.ComposedScheduler

abstract class SingleUseCase<P, R>(private val composedScheduler: ComposedScheduler?) {

    private var mSingle: Single<R>? = null

    open fun execute(param: P): Single<R> {
        if (mSingle == null) {
            mSingle = buildUseCaseObservable(param)
            if (composedScheduler != null) {
                mSingle = mSingle?.compose(composedScheduler.scheduleSingle())
            }

            mSingle = mSingle?.cache()?.doFinally { mSingle = null }
        }
        return mSingle!!
    }

    protected abstract fun buildUseCaseObservable(model: P): Single<R>
}
