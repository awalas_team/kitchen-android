package pl.awalas.kitchen.domain.utils

import java.util.Calendar
import java.util.Date

fun getTemplateDate(): Date {
    val cal: Calendar = Calendar.getInstance()
    cal.set(DEFAULT_YEAR, 1, 1, 0, 0)
    return cal.time
}

private const val DEFAULT_YEAR = 2000
